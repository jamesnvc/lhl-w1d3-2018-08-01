//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 01-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Greeter.h"
#import "AdSupportedGreeter.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        Greeter* calc1 = [[Greeter alloc] initWithName:@"First One"];
//        calc1.name = @"First One";
        // play around with swapping the names of the types below:
        // what happens when calc2 is declared as Calculator*?
        // what happens when calc2 is declared AdSupportedCalculator & we call `alloc, init` on Calculator?
        AdSupportedGreeter* calc2 = [[AdSupportedGreeter alloc]
                                        initWithName:@"Second One"
                                        sponsor:@"Squarespace"];
//        calc2.history = [@[] mutableCopy];
        calc2.history = [[NSMutableArray alloc] init];
//        calc2.name = @"Second One";
//        calc2.sponsor = @"Casper mattresses";
        // [calc2 setSponsor:@"Casper mattresses"];

        [calc1 sayHi];
        [calc2 sayHi];

        AdSupportedGreeter *calc3 = [[AdSupportedGreeter alloc]
                                        initWithName:@"Third One"];
        [calc3 sayHi];

        NSLog(@"history 1: %@", calc1.history);
        NSLog(@"history 2: %@", calc2.history);
        NSLog(@"history 3: %@", calc3.history);

    }
    return 0;
}
