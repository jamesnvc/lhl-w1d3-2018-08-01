//
//  Calculator.m
//  OOPDemo
//
//  Created by James Cash on 01-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Greeter.h"

@implementation Greeter

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)sayHi
{
    // we don't need to explicitly check if history is nil here, because if it is, this just does nothing
    [self.history addObject:[NSDate date]];
    NSLog(@"Hello, I'm %@; called me %ld times", self.name,
          // same here; if history is nil, [history count] -> nil and nil ~ 0 when we treat it like a number
          [self.history count]);
}

@end
