//
//  AdSupportedGreeter.m
//  OOPDemo
//
//  Created by James Cash on 01-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "AdSupportedGreeter.h"

@implementation AdSupportedGreeter

- (instancetype)initWithName:(NSString *)name sponsor:(NSString *)sponsor
{
    self = [super initWithName:name];
    if (self) {
        _sponsor = sponsor;
    }
    return self;
}

- (void)sponsorMessage {
    if (self.sponsor != nil) {
        NSLog(@"Brought to you by %@", self.sponsor);
    } else {
        NSLog(@"Your ad here");
    }
}

- (void)sayHi
{
    NSLog(@"-----------------");
    [super sayHi];
    [self sponsorMessage];
    NSLog(@"-----------------");
}

@end
