//
//  Calculator.h
//  OOPDemo
//
//  Created by James Cash on 01-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Greeter : NSObject

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSMutableArray* history;

- (instancetype)initWithName:(NSString*)name;
- (void)sayHi;


@end
