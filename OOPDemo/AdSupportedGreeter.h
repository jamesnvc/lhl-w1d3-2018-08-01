//
//  AdSupportedCalculator.h
//  OOPDemo
//
//  Created by James Cash on 01-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Greeter.h"

@interface AdSupportedGreeter : Greeter

@property (nonatomic,strong) NSString* sponsor;

- (instancetype)initWithName:(NSString*)name sponsor:(NSString*)sponsor;

@end
